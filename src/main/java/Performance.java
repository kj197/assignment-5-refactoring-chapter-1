/**
 * 
 */

/**
 * @author Kangjie Ji
 *
 */
public class Performance {
	private String playID;
	private int audience;
	private Play play;
	private int amount;
	private int volumeCredits;

	public Performance(String playID, int audience) {
		this.playID = playID;
		this.audience = audience;
	}

	public String getPlayID() {
		return playID;
	}

	public void setPlayID(String playID) {
		this.playID = playID;
	}

	public int getAudience() {
		return audience;
	}

	public void setAudience(int audience) {
		this.audience = audience;
	}

	public void setPlay(Play play) {
		this.play = play;
	}

	public Play getPlay() {
		return play;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setVolumeCredit(int volumeCredit) {
		this.volumeCredits = volumeCredit;
	}

	public int getVolumeCredits() {
		return volumeCredits;
	}

}
