import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Kangjie Ji
 *
 */




public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}


	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}


	public int amountFor(Performance perf) {
		int thisAmount = 0;
		switch (perf.getPlay().getType()) {
			case "tragedy": thisAmount = 40000;
				if (perf.getAudience() > 30) {
					thisAmount += 1000 * (perf.getAudience() - 30);
				}
				break;
			case "comedy":  thisAmount = 30000;
				if (perf.getAudience() > 20) {
					thisAmount += 10000 + 500 * (perf.getAudience() - 20);
				}
				thisAmount += 300 * perf.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  perf.getPlay().getType());
		}
		return thisAmount;
	}

	public Play playFor(Performance perf){
		return plays.get(perf.getPlayID());
	}

	public int totalVolumeCredits(StatementData data) {
		int volumeCredits = 0;
		for (Performance perf: data.performances) {
			volumeCredits += perf.getVolumeCredits();
		}
		return volumeCredits;
	}

	public int volumeCreditsFor(Performance perf) {
		int result = 0;
		result += Math.max(perf.getAudience() - 30, 0);
		if ("comedy" == perf.getPlay().getType()) result += Math.floor(perf.getAudience() / 5);
		return result;
	}

	public String usd(double aNumber) {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		String result =numberFormat.format((double)aNumber/100);
		return result;
	}

	public int totalAmount(StatementData data) {
		int result = 0;
		for (Performance perf: data.performances) {
			result += perf.getAmount();
		}
		return result;
	}

	public Performance enrichPerformance(Performance aPerformance) {
		Performance perf2;
		perf2 = aPerformance;
		perf2.setPlay(playFor(perf2));
		perf2.setAmount(amountFor(perf2));
		perf2.setVolumeCredit(volumeCreditsFor(perf2));
		return perf2;
	}


	public StatementData createStatementData(){
		StatementData data = new StatementData(this.customer, this.performances);
		for (Performance perf: data.performances){
			enrichPerformance(perf);
		}

		data.setTotalAmount(totalAmount(data));
		data.setTotalVolumeCredits(totalVolumeCredits(data));
		return data;
	}


	public String renderPlainText(StatementData data) {
		String result = "Statement for " + this.customer + "\n";

		for (Performance perf: data.performances) {

			//enrichPerformance(perf);

			if (playFor(perf) == null) {
				throw new IllegalArgumentException("No play found");
			}

			// print line for this order
			result += "  " + perf.getPlay().getName() + ": $" + usd(perf.getAmount() ) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}

		result += "Amount owed is $" + usd(data.getTotalAmount() ) + "\n";
		result += "You earned " + data.getTotalVolumeCredits() + " credits" + "\n";
		return result;
	}

	public String statement() {
		return renderPlainText(createStatementData());
	}



	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
